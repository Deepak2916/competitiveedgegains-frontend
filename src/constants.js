import translations from "./en.json";
export const signupOrSigninFields = [
  {
    label: translations.lables.userName,
    name: "userName",
    value: "",
    type: "text",
    loginFeild: false,
  },
  {
    label: translations.lables.email,
    name: "email",
    value: "",
    type: "email",
    loginFeild: true,
  },
  {
    label: translations.lables.phoneNumber,
    name: "phoneNumber",
    value: "",
    type: "number",
    loginFeild: false,
  },
  {
    label: translations.lables.password,
    name: "password",
    value: "",
    type: "password",
    loginFeild: true,
  },
  {
    label: translations.lables.confirmPassword,
    name: "confirmPassword",
    value: "",
    type: "password",
    loginFeild: false,
  },
];

export const notifications = [
  {
    id: "ssc",
    name: "SSC",
    url: "/notifications/ssc",
  },
  {
    id: "bank",
    name: "Bank",
    url: "/notifications/bank",
  },
];
const subjects = [
  {
    id: "ssc-aptitude",
    name: "Aptitude",
    url: "test/ssc/aptitude",
  },
  {
    id: "ssc-reasoning",
    name: "Reasoning",
    url: "test/ssc/resoning",
  },
  {
    id: "ssc-english",
    name: "English",
    url: "test/ssc/english",
  },
  {
    id: "general-knowledge",
    name: "General Knowledge",
    url: "test/ssc/gk",
  },
  {
    id: "current-affairs",
    name: "Current Affairs",
    url: "test/ssc/ca",
  },
];

const userManagement = [
  {
    id: "users",
    name: "Users",
    url: "/userManagement/student",
  },
  {
    id: "masters",
    name: "Staff",
    url: "/userManagement/master",
  },
  {
    id: "admin",
    name: "Admins",
    url: "/userManagement/admin",
  },
];

export const initialSignupData = {
  userName: "",
  email: "",
  phoneNumber: "",
  password: "",
  confirmPassword: "",
  selectedOptions: "",
};

export const initialUserData = {
  isUserFetched: false,
  role: "user",
  isAuthenticated: false,
  userName: "",
  email: "",
  phoneNumber: "",
  selectedOptions: [],
  profileImg: "",
};

export const homePageHeader = {
  title: translations.headerTitle,
};

export const menu = [
  {
    id: "home",
    name: "Home",
    hasAccordion: false,
    role: ["student", "admin", "super admin", "master"],
    url: "/",
  },
  {
    id: "dashboard",
    name: "Dashboard",
    hasAccordion: false,
    role: ["student", "master"],
    url: "/dashboard",
  },
  {
    id: "adminDashboard",
    name: "Admin Dashboard",
    hasAccordion: false,
    role: ["admin", "super admin"],
    url: "/admin/dashboard",
  },
  {
    id: "userManagement",
    name: "User Management",
    hasAccordion: true,
    list: userManagement,
    role: ["admin", "super admin", "admin"],
    url: "/admin/userManagemant",
  },
  // {
  //   id: "exams",
  //   name: "Exams",
  //   role: ["user", "admin", "developer"],
  //   hasAccordion: true,
  //   url: "/exams",
  // },
  {
    id: "subjects",
    name: "Subjects",
    role: ["student", "admin", "super admin", "master"],
    list: subjects,
    hasAccordion: true,
    url: "/:exam/:subject",
  },
  {
    id: "notifications",
    name: "Notifications",
    hasAccordion: true,
    list: notifications,
    role: ["student", "admin", "super admin", "master"],
    url: "/notifications",
  },
];

export const footerMenu = [
  {
    id: "about",
    name: "About",
    hasAccordion: false,
    role: ["user", "admin", "super admin", "master"],
    url: "/about",
  },
  {
    id: "contact",
    name: "Conatct us",
    hasAccordion: false,
    role: ["user", "admin", "super admin", "master"],
    url: "/contact",
  },
  {
    id: "feedback",
    name: "Feedback",
    hasAccordion: false,
    role: ["student", "admin", "super admin", "master"],
    url: "/feedback",
  },
  {
    id: "logout",
    name: "Logout",
    hasAccordion: false,
    role: ["student", "admin", "super admin", "master"],
    url: "/user/signin",
  },
];

export const publicRoutes = ["user/signin", "user/signout", "/"];
