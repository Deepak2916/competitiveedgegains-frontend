import { Button, Chip, Typography } from "@mui/material";
import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import QuestionComponent from "./QuestionComponent";
import DoneIcon from "@mui/icons-material/Done";
import ClearIcon from "@mui/icons-material/Clear";
import Avatar from "@material-ui/core/Avatar";
import { deepOrange, deepPurple } from "@material-ui/core/colors";
import { MTableToolbar } from "material-table";

export default function Elements(data, rowData) {
  const { style, title, variant, onClick } = data;

  const elements = {
    button: () => (
      <Button key={title} sx={style} variant={variant} onClick={onClick}>
        {title}
      </Button>
    ),
    text: () => (
      <Typography key={title} variant={variant} style={style}>
        {title}
      </Typography>
    ),
    delete: () => (
      <DeleteIcon
        key={title}
        onClick={() => onClick(rowData)}
        fontSize="large"
        sx={style}
      />
    ),
    add: () => (
      <AddIcon
        key={title}
        onClick={() => onClick(rowData)}
        fontSize="large"
        sx={style}
      />
    ),
    done: () => (
      <DoneIcon
        key={title}
        onClick={onClick && (() => onClick(rowData))}
        fontSize="large"
        sx={{
          ...style,
        }}
      />
    ),
    clear: () => (
      <ClearIcon
        key={title}
        onClick={onClick && (() => onClick(rowData))}
        fontSize="large"
        sx={style}
      />
    ),
    // avatar: (
    //   <div
    //     style={{
    //       display: "flex",
    //       flexDirection: "column",
    //       columnGap: "10px",
    //     }}
    //   >
    //     <Avatar
    //       alt="User Avatar"
    //       src={rowData.imageUrl || ""}
    //       style={{
    //         backgroundColor: rowData.createdUser?.imageUrl
    //           ? "transparent"
    //           : deepOrange[500],
    //         color: rowData.createdUser?.imageUrl ? "inherit" : "#fff",
    //       }}
    //     >
    //       {!rowData.imageUrl && "U"}
    //     </Avatar>
    //     <Typography variant="h6">{rowData.createdUser?.name}</Typography>
    //   </div>
    // ),
    questions: () =>
      rowData?.questions?.length ? (
        <QuestionComponent rowData={rowData} onEdit={onClick} />
      ) : null,

    toolbar: (data) => (
      <div style={data?.style?.div}>
        {data?.chips?.map((chip) => {
          return (
            <Chip
              label={chip.name}
              color={chip.id === data.selectedChip ? "secondary" : "default"}
              style={data.style.chip}
              onClick={() => data.onClick(chip.id)}
            />
          );
        })}
      </div>
    ),
  };

  return elements[data.element];
}
