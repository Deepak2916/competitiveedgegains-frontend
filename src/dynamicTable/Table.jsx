import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";

import Elements from "./Elements";
import { Chip } from "@mui/material";

export default function Table({
  data,
  header,
  columns,
  actions = [],
  options,
  customActions,
  detailPanel,
  components,
}) {
  let tableColumns = columns;
  const newActions = actions
    ? actions.map((action) => {
        if (action.icon.type === "element") {
          return {
            ...action,
            icon: (rowData) =>
              action.icon.data ? Elements(action.icon.data, rowData)() : null,
          };
        } else {
          return {
            ...action,
            icon: action.icon.data,
          };
        }
      })
    : null;

  const newActionColums = (customActions) => ({
    title: customActions?.title,
    feild: "action",
    render: (rowData) => {
      return (
        <div
          style={{
            display: "flex",
            columnGap: "10px",
          }}
        >
          {customActions?.elements.map((data) =>
            data ? Elements(data, rowData)() : null
          )}
        </div>
      );
    },
  });
  const newDetailPanel = detailPanel?.map((data) => {
    return {
      tooltip: data.tooltip,
      render: (rowData) => (data ? Elements(data, rowData)() : null),
    };
  });
  const getComponents = (components) => {
    return Elements(components)();
  };

  if (customActions) {
    tableColumns = [...columns, newActionColums(customActions)];
  }
  return (
    <MaterialTable
      style={{
        padding: "0 10px",
        borderRadius: "10px",
      }}
      title={
        <div style={header?.style?.container}>
          <h2 style={header?.style?.title}>{header.title}</h2>
          <p style={header?.style?.description}>{header.description}</p>
        </div>
      }
      columns={tableColumns}
      data={data}
      actions={newActions}
      options={{
        ...options,
        headerStyle: {
          backgroundColor: "#01579b",
          color: "#FFF",
          padding: "10px",
        },
        rowStyle: {
          backgroundColor: "#EEE",
        },
        // detailPanelColumnAlignment: "right",
      }}
      detailPanelStyle={{
        backgroundColor: "#f9f9f9",
        padding: "10px",
        width: "10px",
        boxSizing: "border-box",
      }}
      detailPanel={newDetailPanel}
      components={{
        Toolbar: (props) => (
          <div
            style={{
              margin: "10px 0",
            }}
          >
            <MTableToolbar {...props} />

            {components && Elements(components)(components.data)}
          </div>
        ),
      }}
    />
  );
}
