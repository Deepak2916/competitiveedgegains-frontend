import React, { useState } from "react";
import {
  Paper,
  Typography,
  Radio,
  RadioGroup,
  FormControl,
  FormControlLabel,
  Button,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
export default function QuestionComponent({ rowData, onEdit }) {
  return (
    <>
      <div
        key={rowData.id}
        style={{
          padding: "10px 50px",
        }}
      >
        {rowData.questions.map((data, index) => (
          <>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h5" gutterBottom>
                {index + 1}. {data.question}
              </Typography>
              <EditIcon
                style={{
                  fontSize: "2rem",
                  cursor: "pointer",
                  color: "black",
                  transition: "color 0.3s ease-in-out",
                }}
                onClick={() => {
                  onEdit({ data, rowData });
                }}
              />
            </div>

            <FormControl component="fieldset" sx={{ padding: "0 20px" }}>
              <Typography variant="h7" gutterBottom>
                Difficulty Level: <b>{data?.difficulty.toUpperCase()}</b>
              </Typography>
              <RadioGroup
                aria-label="options"
                name="options"
                value={data.correctOption}
              >
                {data.options.map((option, index) => (
                  <FormControlLabel
                    key={index}
                    value={option}
                    control={<Radio />}
                    label={option}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          </>
        ))}
      </div>
    </>
  );
}
