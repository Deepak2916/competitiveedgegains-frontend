import { useEffect } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Routes, Route, useNavigate } from "react-router-dom";
import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache,
  useQuery,
} from "@apollo/client";
import { useDispatch, useSelector } from "react-redux";
import SignupAndSignin from "./components/SignupAndSignin";
import Menu from "./components/Menu";
import { getTokenCookie } from "./utilities/cookie";
import { GET_LOGIN_USER_DATA } from "./graphql/user";
import { loginSuccess } from "./store/userSlice";
import SkeletonLoader from "./components/Loader";

function App() {
  const theme = createTheme();
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { loading, error, data } = useQuery(GET_LOGIN_USER_DATA);
  const navigate = useNavigate();
  useEffect(() => {
    const response = data ? data.getLoginUserData : {};
    if (!loading && !error) {
      if (response.success) {
        dispatch(loginSuccess(response.user));
      } else {
        dispatch(loginSuccess(null));
        // navigate("user/signin");
      }
    }
  }, [dispatch, loading]);

  if (loading || !user.isUserFetched) {
    return <SkeletonLoader />;
  }

  return (
    <ThemeProvider theme={theme}>
      <Menu />
      <Routes>
        <Route path="/user/:role/:type" element={<SignupAndSignin />} />
      </Routes>
    </ThemeProvider>
  );
}

export default App;
