import { gql } from "@apollo/client";

export const LOGIN_USER_MUTATION = gql`
  mutation LoginUser($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      message
      success
      user {
        _id
        email
        phoneNumber
        role
        points
        profileImg
      }
      jwt
    }
  }
`;
export const SIGNUP_USER_MUTATION = gql`
  mutation CreateUser(
    $username: String!
    $email: String!
    $password: String
    $phoneNumber: String
    $role: String
  ) {
    createUser(
      username: $username
      email: $email
      password: $password
      phoneNumber: $phoneNumber
      role: $role
    ) {
      success
      message
    }
  }
`;

export const GET_LOGIN_USER_DATA = gql`
  query getLoginUserData {
    getLoginUserData {
      success
      message
      user {
        _id
        email
        phoneNumber
        role
        points
        profileImg
      }
      jwt
    }
  }
`;

export const GET_USERS_BY_ROLE = gql`
  query GetUsersByRole($role: String) {
    getUsersByRole(role: $role) {
      success
      message
      users {
        _id
        username
        email
        role
        profileImg
        phoneNumber
        isApprovedByAdmin
      }
    }
  }
`;
