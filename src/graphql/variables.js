export const prepareUserVariables = (type, data, role) => {
  if (type === "login") {
    return {
      variables: {
        email: data.email,
        password: data.password,
      },
    };
  } else {
    return {
      variables: {
        username: data.userName,
        email: data.email,
        password: data.password,
        phoneNumber: data.phoneNumber,
        role,
      },
    };
  }
};

export const prepareGetUsersByRoleVariables = (role) => {
  return {
    variables: {
      role,
    },
  };
};
export const prepareQuestionsSetVariables = (data) => {
  return {
    variables: {
      subject: data.subject,
      exam: data.exam,
    },
  };
};

export const prepareQuestionUpdateVariables = (data, isEdit) => {
  return {
    variables: {
      id: data.id,
      update: {
        ...data.update,
      },
      isEdit,
    },
  };
};

export const prepareQuestionSetUpdateVariables = (id, status) => {
  return {
    variables: {
      id: id,
      update: {
        status,
      },
    },
  };
};
