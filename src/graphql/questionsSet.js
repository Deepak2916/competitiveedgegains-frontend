import { gql } from "@apollo/client";

export const CREATE_QUESTION_SET = gql`
  mutation CreateQuestionSet($subject: String!, $exam: String!) {
    createQuestionSet(subject: $subject, exam: $exam) {
      success
      message
      data {
        _id
        subject
        exam
        status
        points
        total
        hard
        easy
        medium
        questions {
          slNo
          difficulty
          question
          options
        }
      }
    }
  }
`;

export const GET_QUESTION_SETS = gql`
  query GetQuestionSet($subject: String!, $exam: String!) {
    getQuestionSetsByCreatedUserId(subject: $subject, exam: $exam) {
      success
      message
      data {
        _id
        createdUserId
        createdUser {
          email
          name
        }
        subject
        exam
        status
        points
        questions {
          slNo
          difficulty
          question
          options
          correctOption
        }
        total
        hard
        easy
        medium
      }
    }
  }
`;

export const DELETE_QUSTION_SET = gql`
  mutation deleteQuestionSetsById($id: String) {
    deleteQuestionSetsById(id: $id) {
      success
      message
    }
  }
`;

export const UPDATE_QUESTION_SET = gql`
  mutation updateQuestionSetById(
    $id: String!
    $update: UpdateInput!
    $isEdit: Boolean
  ) {
    updateQuestionSetById(id: $id, update: $update, isEdit: $isEdit) {
      success
      message
      data {
        _id
        subject
        exam
        status
        points
        total
        hard
        easy
        medium
        questions {
          slNo
          difficulty
          question
          options
          correctOption
        }
      }
    }
  }
`;
/* 
      success
      message
      data {
        createdUserId
        createdUser {
          name
          email
        }
        subject
        exam
        difficulty
        totalQuestions
        submittedAt
        acceptedAt
        acceptedUserId
        acceptedUser {
          name
          email
        }
        status
        points
        questions {
          question
          options {
            id
            value
          }
        }
        questionSummary {
          total
          hard
          easy
          medium
        }
      }
*/
