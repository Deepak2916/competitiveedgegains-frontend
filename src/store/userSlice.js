import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { initialUserData } from "../constants";
import { removeTokenCookie } from "../utilities/cookie";

export const signupAsync = createAsyncThunk("user/create", async (data) => {
  return {
    type: "signup",
    success: true,
  };
});

export const siginAsync = createAsyncThunk("user/login", async (data) => {
  return {
    type: "signin",
    success: true,
  };
});

const userSlice = createSlice({
  name: "user",
  initialState: { ...initialUserData },
  reducers: {
    loginSuccess: (state, { payload }) => {
      if (payload) {
        state = {
          isUserFetched: true,
          isAuthenticated: true,
          ...payload,
        };
      } else {
        state = {
          ...state,
          isUserFetched: true,
        };
      }
      return state;
    },
    signOut: (state) => {
      removeTokenCookie();
      return { ...initialUserData, isUserFetched: true };
    },
  },
});

export const { signOut, loginSuccess } = userSlice.actions;
export default userSlice.reducer;
