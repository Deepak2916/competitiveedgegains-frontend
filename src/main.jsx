import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { Provider } from "react-redux";
import store from "./store/store.js";
import { BrowserRouter as Router } from "react-router-dom";
import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "./utilities/apollo.js";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Router>
    <Provider store={store}>
      <React.StrictMode>
        <ApolloProvider client={apolloClient}>
          <App />
        </ApolloProvider>
      </React.StrictMode>
    </Provider>
  </Router>
);
