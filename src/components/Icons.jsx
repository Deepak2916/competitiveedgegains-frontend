import React from "react";
import HomeIcon from "@mui/icons-material/Home";
import DashboardIcon from "@mui/icons-material/Dashboard";
import DashboardCustomizeIcon from "@mui/icons-material/DashboardCustomize";
import InfoIcon from "@mui/icons-material/Info";
import ContactPageIcon from "@mui/icons-material/ContactPage";
import FeedbackIcon from "@mui/icons-material/Feedback";
import LogoutIcon from "@mui/icons-material/Logout";
import ComputerIcon from "@mui/icons-material/Computer";
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import SubjectIcon from "@mui/icons-material/Subject";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
function Icons({ id }) {
  const icons = {
    home: <HomeIcon />,
    dashboard: <DashboardIcon />,
    adminDashboard: <DashboardCustomizeIcon />,
    exams: <ComputerIcon />,
    subjects: <SubjectIcon />,
    notifications: <CircleNotificationsIcon />,
    about: <InfoIcon />,
    contact: <ContactPageIcon />,
    feedback: <FeedbackIcon />,
    logout: <LogoutIcon />,
    userManagement: <ManageAccountsIcon />,
  };
  return <>{icons[id]}</>;
}

export default Icons;
