import React from "react";

import styled from "@emotion/styled";

const useStyles = styled((theme) => ({
  skeletonItem: {
    marginBottom: "10px",
    width: "80%",
    height: "20px",
    borderRadius: "4px",
    backgroundColor: theme.palette.action.hover, // Customize the background color
  },
}));

const SkeletonLoader = ({ count = 1 }) => {
  const classes = useStyles();

  return (
    <div>
      {[...Array(count)].map((_, index) => (
        <div key={index} className={classes.skeletonItem}></div>
      ))}
    </div>
  );
};

export default SkeletonLoader;
