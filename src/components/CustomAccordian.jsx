import * as React from "react";
import { styled } from "@mui/material/styles";
import MuiAccordion from "@mui/material/Accordion";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  AccordionSummary,
  ListItemIcon,
  ListItemText,
  AccordionDetails,
} from "@mui/material";
import Icons from "./Icons";
import CunstomList from "./CunstomList";
import { Padding } from "@mui/icons-material";

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&::before": {
    display: "none",
  },
}));

export default function CustomizedAccordions({ item, list }) {
  const [expanded, setExpanded] = React.useState("panel1");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  return (
    <Accordion
      expanded={expanded === "panel1"}
      onChange={handleChange("panel1")}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1d-content"
        id="panel1d-header"
      >
        <div style={{ margin: "0 10px" }}>
          <Icons id={item.id} />
        </div>

        <ListItemText primary={item.name} />
      </AccordionSummary>
      <AccordionDetails sx={{ paddingTop: 0 }}>
        <CunstomList menuList={list} hasIcons={false} />
      </AccordionDetails>
    </Accordion>
  );
}
