import React, { useEffect, useState } from "react";
import Table from "../../dynamicTable/Table";
import { useNavigate, useParams } from "react-router-dom";
import {
  allUsersColumns,
  allUsersOptions,
  allUsersActions,
  allUsersCustomActions,
  allUsersDetailPanel,
  components,
} from "./subjectsTableData";
import { Button, Skeleton } from "@mui/material";
import { useMutation, useQuery } from "@apollo/client";
import {
  GET_QUESTION_SETS,
  CREATE_QUESTION_SET,
  DELETE_QUSTION_SET,
  UPDATE_QUESTION_SET,
} from "../../graphql/questionsSet";
import {
  prepareQuestionSetUpdateVariables,
  prepareQuestionUpdateVariables,
  prepareQuestionsSetVariables,
} from "../../graphql/variables";
import { prepareMasterTableData } from "../../utilities/tableData";
import AddQuestions from "./AddQuestions";
import { useSelector } from "react-redux";

export default function Subjects() {
  const user = useSelector((state) => state.user);
  const { exam, subject } = useParams();
  const navigate = useNavigate();
  const [QuestionSetsData, setquestionSetsData] = useState([]);

  const [questionData, setQuestionData] = useState({
    open: false,
    rowData: {},
  });
  const options = allUsersOptions[user.role];
  const columns = allUsersColumns[user.role];
  const actions = allUsersActions[user.role];
  const detailPanel = allUsersDetailPanel[user.role];
  const customActions = allUsersCustomActions[user.role];

  const [createQuestionSet, {}] = useMutation(CREATE_QUESTION_SET);
  const [deleteQuestionSet, {}] = useMutation(DELETE_QUSTION_SET);
  const [updateQuestionSet, {}] = useMutation(UPDATE_QUESTION_SET);
  const { loading, error, data, refetch } = useQuery(
    GET_QUESTION_SETS,
    prepareQuestionsSetVariables({ exam, subject })
  );
  useEffect(() => {
    refetch();
  }, [subject, exam, refetch]);

  useEffect(() => {
    const response = data ? data.getQuestionSetsByCreatedUserId : {};

    if (!loading && response.success) {
      setquestionSetsData(prepareMasterTableData(response.data));
    }
  }, [loading, data]);

  let title = "";
  if (exam == "common") {
    title = subject === "ca" ? "Current Affairs" : "General Knowledge";
  } else {
    title = `${exam.toUpperCase()} ${subject.toUpperCase()}`;
  }
  if (loading) {
    return <Skeleton />;
  }

  const handleOpenDialog = (rowData) => {
    setQuestionData({
      open: true,
      rowData,
    });
  };

  const handleCloseDialog = () => {
    setQuestionData({
      open: false,
      rowData: {},
    });
  };

  const saveQuestion = async (data, isEdit) => {
    const {
      data: { updateQuestionSetById: updateResponse },
    } = await updateQuestionSet(prepareQuestionUpdateVariables(data, isEdit));
    if (updateResponse.success) {
      setquestionSetsData((oldData) => {
        const newData = oldData.map((data) => {
          if (data._id === updateResponse.data[0]._id) {
            return updateResponse.data[0];
          }
          return data;
        });
        return newData;
      });
    }
  };
  const addQuestionHandler = (rowData) => {
    handleOpenDialog(rowData);
  };
  const deleteStetsHandler = async (rowData) => {
    try {
      const {
        data: { deleteQuestionSetsById: deleteResponse },
      } = await deleteQuestionSet({
        variables: {
          id: rowData._id,
        },
      });

      if (deleteResponse.success) {
        setquestionSetsData((oldData) => {
          const newData = oldData.filter((data) => {
            return data._id !== rowData._id;
          });
          return newData;
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const addSetHandler = async (event, rowData) => {
    try {
      const {
        data: { createQuestionSet: createResponse },
      } = await createQuestionSet(
        prepareQuestionsSetVariables({ subject: subject, exam })
      );

      if (createResponse.success) {
        setquestionSetsData((oldData) => [...oldData, ...createResponse.data]);
      } else {
        console.log(createResponse);
      }
    } catch (error) {
      console.log(error.message);
    }
  };
  const editQuestionHandler = ({ data, rowData }) => {
    setQuestionData({
      open: true,
      rowData: {
        ...data,
        hard: rowData.hard,
        medium: rowData.medium,
        easy: rowData.easy,
        id: rowData._id,
      },
      isEdit: true,
    });
  };

  const rejectQuestionSet = (data, rowData) => {
    updateQuestionSet(
      prepareQuestionSetUpdateVariables(rowData._id, "rejected")
    );
  };

  const acceptQuestionSet = (data, rowData) => {
    if (rowData.total === 25) {
      updateQuestionSet(
        prepareQuestionSetUpdateVariables(rowData._id, "accepted")
      );
    }
  };
  const actionFun = {
    addQuestion: addQuestionHandler,
    deleteQuestion: deleteStetsHandler,
    addSet: addSetHandler,
    editQuestion: editQuestionHandler,
    reject: rejectQuestionSet,
    accept: acceptQuestionSet,
  };
  const prepareAction = (action = [], name) => {
    return action.map((data) => ({
      ...data,
      onClick: actionFun[data?.onClick],
    }));
  };
  const ChipOnClickHandler = (exam) => {
    navigate(`/test/${exam}/${subject}`);
  };

  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Table
        columns={columns}
        data={QuestionSetsData}
        header={{
          title,
          description: "Each set must have 25 questions",
          style: {
            title: {},
            description: {},
            container: {},
          },
        }}
        actions={prepareAction(actions, "actions")}
        options={options}
        customActions={
          customActions
            ? {
                ...customActions,
                elements: prepareAction(
                  customActions?.elements,
                  "customActions"
                ),
              }
            : null
        }
        detailPanel={prepareAction(detailPanel, "detailPanel")}
        components={{
          ...components,
          data: {
            ...components.data,
            selectedChip: exam,
            subject: subject,
            onClick: ChipOnClickHandler,
          },
        }}
      />
      {questionData.open ? (
        <AddQuestions
          key={questionData?.rowData?._id}
          onClose={handleCloseDialog}
          questionData={questionData}
          saveQuestion={saveQuestion}
        />
      ) : null}
    </div>
  );
}
