import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { InputLabel } from "@mui/material";

const AddQuestions = ({ onClose, questionData, saveQuestion }) => {
  const { open, rowData, isEdit } = questionData;

  const questionValue = isEdit ? rowData.question : "";
  const optionsValue = isEdit ? rowData.options : ["", "", "", ""];
  const difficultyValue = isEdit ? rowData.difficulty : "";
  const correctOptionValue = isEdit ? rowData.correctOption : "";

  const [question, setQuestion] = useState(questionValue);
  const [options, setOptions] = useState(optionsValue);
  const [difficulty, setDifficulty] = useState(difficultyValue);
  const [correctOption, setCorrectOption] = useState(correctOptionValue);
  const [errors, setErrors] = useState({});

  const handleOptionChange = (index, value) => {
    const newOptions = [...options];
    newOptions[index] = value;
    setOptions(newOptions);
  };

  const handleDifficultyChange = (event) => {
    setDifficulty(event.target.value);
  };

  const handleCorrectOptionChange = (event) => {
    setCorrectOption(event.target.value);
  };

  const handleSave = () => {
    const newErrors = {};
    if (!question.trim()) {
      newErrors.question = "Question is required";
    }
    const emptyOptionIndex = options.findIndex((option) => !option.trim());
    if (emptyOptionIndex !== -1) {
      newErrors[`option${emptyOptionIndex}`] = `Option ${
        emptyOptionIndex + 1
      } is required`;
    }
    if (!difficulty.trim()) {
      newErrors.difficulty = "Difficulty is required";
    }
    if (!correctOption.trim()) {
      newErrors.correctOption = "Correct option is required";
    }
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }
    let updatedData = {};

    updatedData = {
      id: isEdit ? rowData.id : rowData._id,
      update: {
        slNo: rowData.slNo,
        question,
        options,
        difficulty,
        correctOption,
      },
    };

    saveQuestion(updatedData, isEdit);

    onClose();
  };

  return (
    <Dialog
      key={rowData.id}
      open={open}
      onClose={onClose}
      maxWidth="lg"
      fullWidth
    >
      <DialogTitle>{isEdit ? "Edit Question" : "Add New Question"}</DialogTitle>
      <DialogContent>
        <TextField
          label="Question"
          fullWidth
          margin="normal"
          value={question}
          onChange={(e) => setQuestion(e.target.value)}
          error={Boolean(errors.question)}
          helperText={errors.question}
        />
        {options.map((option, index) => (
          <TextField
            key={index}
            label={`Option ${index + 1}`}
            fullWidth
            margin="normal"
            value={option}
            onChange={(e) => handleOptionChange(index, e.target.value)}
            error={Boolean(errors[`option${index}`])}
            helperText={errors[`option${index}`]}
          />
        ))}
        <div style={{ marginTop: "16px" }}>
          <InputLabel id="select-difficulty">
            Select Difficulty Level
          </InputLabel>
          <RadioGroup row value={difficulty} onChange={handleDifficultyChange}>
            <FormControlLabel
              value="hard"
              disabled={isEdit || rowData.hard >= 5}
              control={<Radio />}
              label="Hard"
            />
            <FormControlLabel
              value="medium"
              control={<Radio />}
              label="Medium"
              disabled={isEdit || rowData.medium >= 10}
            />
            <FormControlLabel
              value="easy"
              disabled={isEdit || rowData.easy >= 10}
              control={<Radio />}
              label="Easy"
            />
          </RadioGroup>
        </div>
        <div style={{ marginTop: "16px" }}>
          <InputLabel id="select-label">Select Correct Option</InputLabel>
          <Select
            labelId="select-label"
            label="Correct Option"
            value={correctOption}
            onChange={handleCorrectOptionChange}
            fullWidth
            margin="normal"
            style={{ marginTop: "16px" }}
            error={Boolean(errors.correctOption)}
            helperText={errors.correctOption}
          >
            {options.map((option, index) => (
              <MenuItem key={index} value={option}>
                Option {index + 1}
              </MenuItem>
            ))}
          </Select>
        </div>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSave}
          style={{ marginTop: "16px" }}
        >
          Save
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default AddQuestions;
