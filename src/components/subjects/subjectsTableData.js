export const allUsersOptions = {
  master: {
    search: false,
    paging: false,
    actionsColumnIndex: -1,
  },
  student: {},
  admin: {
    search: false,
    paging: false,
    actionsColumnIndex: -1,
  },
};

export const allUsersColumns = {
  student: [
    { title: "Exam", field: "exam", sorting: false },
    { title: "Subject", field: "subject", sorting: false },
    {
      title: "Total(25)",
      field: "total",
      with: "50px",
      sorting: false,
    },
    { title: "Total Points", field: "totalPoints", sorting: false },
  ],
  "super admin": [
    { title: "Exam", field: "exam", sorting: false },
    { title: "Subject", field: "subject", sorting: false },
    { title: "Total Questions", field: "total", sorting: false },
    {
      title: "User",
      field: "createdBy",
      render: (rowdata) => rowdata.createdUser.name,
    },
    {
      title: "Email",
      field: "createdBy",
      render: (rowdata) => rowdata.createdUser.email,
    },
    { title: "Status", field: "status" },
    { title: "Total Points", field: "points" },
    { title: "SubmittedAt", field: "submittedAt" },
  ],

  admin: [
    { title: "Exam", field: "exam", sorting: false },
    { title: "Subject", field: "subject", sorting: false },
    { title: "Total Questions", field: "total", sorting: false },
    {
      title: "User",
      field: "createdBy",
      render: (rowdata) => rowdata.createdUser.name,
    },
    {
      title: "Email",
      field: "createdBy",
      render: (rowdata) => rowdata.createdUser.email,
    },
    { title: "Status", field: "status" },
    { title: "Total Points", field: "points" },
    { title: "SubmittedAt", field: "submittedAt" },
  ],
  master: [
    { title: "Exam", field: "exam", sorting: false },
    { title: "Subject", field: "subject", sorting: false },

    { title: "Easy (10)", field: "easy", sorting: false },
    { title: "Medium (10)", field: "medium", sorting: false },
    { title: "Hard (5)", field: "hard", sorting: false },
    { title: "Total (25)", field: "total", sorting: false },

    { title: "Status", field: "status", sorting: false },
  ],
};

export const allUsersActions = {
  master: [
    {
      icon: {
        type: "element",
        data: {
          element: "text",
          title: "+Add",
          style: {
            borderRadius: "20px",
            backgroundColor: "green",
            padding: "1rem",
            color: "#ffff",
            "&:hover": {
              backgroundColor: "darkgreen",
            },
          },
          variant: "h6",
        },
      },
      tooltip: "Add Set",
      isFreeAction: true,
      onClick: "addSet",
    },
  ],
  admin: [
    {
      tooltip: "Approve the set",
      icon: {
        type: "element",
        data: {
          element: "done",
          title: "Add",
          style: {
            color: "green",
            "&:hover": {
              backgroundColor: "green",
              color: "white",
            },
            "&:active": {
              backgroundColor: "lightgreen",
            },
          },
          variant: "contained",
        },
      },
      onClick: "accept",
    },
    {
      tooltip: "Approve the set",
      icon: {
        type: "element",
        data: {
          element: "clear",
          title: "Clear",
          style: {
            color: "red",

            "&:hover": {
              backgroundColor: "red",
              color: "white",
            },
            "&:active": {
              color: "lightcoral",
            },
          },
          variant: "contained",
        },
      },
      onClick: "reject",
    },
  ],
  "super admin": [
    {
      tooltip: "Approve the set",
      icon: {
        type: "element",
        data: {
          element: "done",
          title: "Add",
          style: {
            // color: "green",
            "&:hover": {
              backgroundColor: "green",
              color: "white",
            },
            "&:active": {
              backgroundColor: "lightgreen",
            },
          },
          variant: "contained",
        },
      },
      onClick: "accept",
    },
    {
      tooltip: "Approve the set",
      icon: {
        type: "element",
        data: {
          element: "clear",
          title: "Clear",
          style: {
            color: "red",

            "&:hover": {
              backgroundColor: "red",
              color: "white",
            },
            "&:active": {
              color: "lightcoral",
            },
          },
          variant: "contained",
        },
      },
      onClick: "reject",
    },
  ],
  student: [],
};

export const allUsersCustomActions = {
  master: {
    title: "Actions",
    elements: [
      {
        element: "add",
        title: "Add",
        style: {
          color: "white",
          backgroundColor: "green",
          "&:hover": {
            backgroundColor: "darkgreen",
          },
          "&:active": {
            backgroundColor: "lightgreen",
          },
        },
        variant: "contained",
        onClick: "addQuestion",
      },
      {
        element: "delete",
        title: "Delete",
        style: {
          color: "red",
          "&:hover": {
            color: "darkred",
          },
          "&:active": {
            color: "lightcoral",
          },
        },
        variant: "contained",
        onClick: "deleteQuestion",
      },
    ],
  },
  admin: null,
  "super admin": null,
  student: null,
};

export const allUsersDetailPanel = {
  master: [
    {
      tooltip: "Show Questions",
      element: "questions",
      onClick: "editQuestion",
    },
  ],
  admin: [
    {
      tooltip: "Show Questions",
      element: "questions",
      onClick: "",
    },
  ],
  "super admin": [
    {
      tooltip: "Show Questions",
      element: "questions",
      onClick: "",
    },
  ],
  student: [],
};

export const components = {
  title: "Toolbar",
  element: "toolbar",
  data: {
    selectedChip: "",
    style: {
      div: {
        padding: "0px 10px",
      },
      chip: {
        marginRight: 5,
        width: "100px",
        padding: "10px",
        fontSize: "15px",
        fontWeight: "bold",
      },
    },
    chips: [
      {
        id: "ssc",
        name: "SSC",
      },
      {
        id: "bank",
        name: "BANK",
      },
    ],
  },
};
