import React, { useEffect } from "react";
import CustomizedAccordions from "./CustomAccordian";
import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import Icons from "./Icons";
import { useSelector, useDispatch } from "react-redux";
import { signOut } from "../store/userSlice";
import { Link, useNavigate } from "react-router-dom";
import { removeTokenCookie } from "../utilities/cookie";
// import { subjects } from "../constants";

export default function CunstomList({ menuList, hasIcons }) {
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    if (!user.isAuthenticated) {
      navigate("/home");
    }
  }, [dispatch]);
  const handleOnClick = (item) => {
    if (item.id === "logout") {
      navigate(item.url);
      removeTokenCookie();
      dispatch(signOut());
    } else {
      navigate(item.url);
    }
  };
  // const getList = (id) => {
  //   return id === "exams" || id === "notifications"
  //     ? user.selectedCourses
  //     : subjects;
  // };
  return (
    <List
      sx={{
        padding: 0,
      }}
    >
      {menuList.map((item, index) =>
        item.hasAccordion ? (
          <CustomizedAccordions key={item.id} item={item} list={item.list} />
        ) : (
          <ListItem key={item.id}>
            <ListItemButton
              onClick={() => handleOnClick(item)}
              sx={{ padding: 0 }}
            >
              {hasIcons && (
                <div style={{ margin: "0 10px" }}>
                  <Icons id={item.id} />
                </div>
              )}
              <ListItemText primary={item.name} />
            </ListItemButton>
          </ListItem>
        )
      )}
    </List>
  );
}
