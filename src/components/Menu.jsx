import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";

import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

import AppHeader from "./Header";
import { footerMenu, menu } from "../constants";
import { useSelector } from "react-redux";
import CunstomList from "./CunstomList";
import Home from "./home/Home";
import { Route, Routes, useNavigate } from "react-router-dom";
import Dashboard from "./dashboard/Dashboard";
import PrivateRoutes from "../PrivateRoute";
import Subjects from "./subjects/Subjects";
import UserManagement from "./userManagemant/UserManagement";

const drawerWidth = 270;

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  })
);

const DrawerHeader = styled("div")(({ theme }) => {
  return {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),

    minHeight: 50,
    justifyContent: "flex-end",
  };
});

export default function Menu() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const footerMenuList = footerMenu;
  const user = useSelector((state) => state.user);
  const menuList = menu.filter((data) => data?.role?.includes(user.role));
  const navigate = useNavigate();
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <AppHeader
        openMenu={open}
        handleDrawerOpen={handleDrawerOpen}
        drawerWidth={open && user.isAuthenticated ? drawerWidth : 0}
      />

      {user.isAuthenticated && (
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: drawerWidth,
              boxSizing: "border-box",
            },
          }}
          variant="persistent"
          anchor="left"
          open={open}
        >
          <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </DrawerHeader>
          <Divider />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              height: "100%",
            }}
          >
            <CunstomList menuList={menuList} hasIcons={true} />
            <div>
              <Divider />
              <CunstomList menuList={footerMenuList} hasIcons={true} />
            </div>
          </div>
        </Drawer>
      )}
      <Main
        open={open}
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <DrawerHeader />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            element={<PrivateRoutes isAuthenticated={user.isAuthenticated} />}
          >
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/test/:exam/:subject" element={<Subjects />} />
            <Route path="/userManagement/:role" element={<UserManagement />} />
          </Route>
        </Routes>
      </Main>
    </Box>
  );
}
