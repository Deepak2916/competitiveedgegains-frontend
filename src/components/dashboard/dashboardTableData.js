export const options = {
  search: false,
  paging: false,
};

export const columns = [
  { title: "Exam", field: "exam" },
  { title: "Subject", field: "subject" },
  { title: "Total Questions", field: "Totalquestions" },
  { title: "Duration", field: "duration" },
  { title: "Status", field: "status" },
  { title: "Total Points", field: "totalPoints" },
  { title: "Points Gained", field: "pointsGained" },
  { title: "Attempted on", field: "attemptedAt" },
];

export const actions = [];
