import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Grid,
  TextField,
  Button,
  FormControl,
  Checkbox,
  ListItemText,
  Autocomplete,
  Paper,
  Box,
} from "@mui/material";
import { initialSignupData, signupOrSigninFields } from "../constants";
import { validateForm } from "../utilities/inputValidations";
import translations from "../en.json";
import { useDispatch, useSelector } from "react-redux";

import { useMutation } from "@apollo/client";
import { LOGIN_USER_MUTATION, SIGNUP_USER_MUTATION } from "../graphql/user";
import { prepareUserVariables } from "../graphql/variables";
import { loginSuccess } from "../store/userSlice";
import { setTokenCookie } from "../utilities/cookie";

const SignupAndSignin = () => {
  const initialErrorData = { ...initialSignupData };
  const initialFormData = { ...initialSignupData };

  const { type, role } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  const [loginUser, loginData] = useMutation(LOGIN_USER_MUTATION);
  const [signupUser, signupData] = useMutation(SIGNUP_USER_MUTATION);
  const [formData, setFormData] = useState({ ...initialFormData });
  const [responsError, setResponseError] = useState("");
  const [formErrors, setFormErrors] = useState({ ...initialErrorData });
  let loginPage = true;
  const userRoles = ["student", "admin", "master"];

  useEffect(
    () => {
      if (user.isAuthenticated || !userRoles.includes(role)) {
        navigate("/");
      } else {
      }
    },
    [user.isAuthenticated],
    role
  );
  if (type === "signin") {
    loginPage = true;
  } else if (type === "signup") {
    loginPage = false;
  }

  const getInputFeilds = (loginPage) => {
    return loginPage
      ? signupOrSigninFields.filter((data) => data.loginFeild)
      : signupOrSigninFields;
  };
  const setFormDataFun = (data) =>
    setFormData((prevData) => ({
      ...prevData,
      ...data,
    }));

  const setFormErrorsFun = (data) =>
    setFormErrors((prevErrors) => ({
      ...prevErrors,
      ...data,
    }));

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormDataFun({ [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { isFormValid, errors } = validateForm(formData);

    if (loginPage) {
      if (errors.email) {
        return setFormErrorsFun({ email: errors.email });
      } else {
        const {
          data: { login },
        } = await loginUser(prepareUserVariables("login", formData));

        if (login.success) {
          setTokenCookie(login.jwt);
          dispatch(loginSuccess(login.user));
          return navigate("/");
        } else {
          setResponseError(login.message);
        }
      }
    }
    if (isFormValid && !loginPage) {
      const {
        data: { createUser },
      } = await signupUser(prepareUserVariables("signup", formData, role));
      if (createUser.success) {
        return navigate(`/user/${role}/signin`);
      } else {
        setResponseError(createUser.message);
      }
    } else {
      return setFormErrors((prevErrors) => ({
        ...prevErrors,
        ...errors,
      }));
    }
  };

  const handleAutocompleteChange = (event, value) => {
    setFormData((prevData) => ({
      ...prevData,
      selectedOptions: value,
    }));
  };
  return (
    <Box>
      <Paper
        elevation={10}
        style={{
          padding: "2rem",
          maxWidth: 400,
          margin: "10px auto",
          borderRadius: "13px",
        }}
      >
        <p
          style={{
            color: "red",
            textAlign: "center",
          }}
        >
          {responsError}
        </p>
        <form
          onSubmit={handleSubmit}
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            rowGap: "20px",
          }}
        >
          <h1
            style={{
              textAlign: "center",
              marginBottom: "20px",
              fontSize: "24px",
              fontWeight: "bold",
            }}
          >
            {loginPage ? "Welcome Back!" : "Create an Account"}
          </h1>
          {getInputFeilds(loginPage).map((data) => (
            <TextField
              key={data.name}
              fullWidth
              label={data.label}
              name={data.name}
              type={data.type}
              value={formData[data.name]}
              onChange={handleChange}
              error={formErrors[data.name] !== ""}
              helperText={!loginPage && formErrors[data.name]}
              variant="outlined"
              style={{ borderRadius: "10px" }}
            />
          ))}
          {loginPage && (
            <Button
              style={{
                alignSelf: "flex-end",
                margin: "0",
                padding: "0",
                color: "#4CAF50",
              }}
            >
              {translations.forgotPassword}
            </Button>
          )}

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "1.5rem",
              marginTop: "1.5rem",
              alignItems: "center",
            }}
          >
            <Button
              type="submit"
              style={{
                minWidth: "150px",
                minHeight: "50px",
                fontSize: "18px",
                fontWeight: "bold",
                backgroundColor: "#4CAF50",
                borderRadius: "8px",
                color: "white",
              }}
              variant="contained"
              color="primary"
            >
              {loginPage ? "Sign In" : "Sign Up"}
            </Button>
            <p style={{ textAlign: "center", fontSize: "14px", color: "#333" }}>
              {loginPage
                ? translations.signUpPrompt
                : translations.signInPrompt}
            </p>
            <Button
              onClick={() => {
                if (loginPage) {
                  navigate(`/user/${role}/signup`);
                } else {
                  navigate(`/user/${role}/signin`);
                }
                setFormErrors({ ...initialErrorData });
                setFormData({ ...initialFormData });
              }}
              style={{ fontSize: "16px", color: "#4CAF50", fontWeight: "bold" }}
            >
              {loginPage ? "Create an Account" : "Sign In"}
            </Button>
          </div>
        </form>
      </Paper>
    </Box>
  );
};

export default SignupAndSignin;
