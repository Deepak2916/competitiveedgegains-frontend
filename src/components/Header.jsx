import React, { useState } from "react";
import {
  Toolbar,
  Typography,
  Avatar,
  Button,
  Popover,
  Paper,
  Box,
  Input,
  IconButton,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useNavigate } from "react-router-dom";
import translations from "../en.json";
import { Edit as EditIcon } from "@mui/icons-material";
import { styled } from "@mui/system";
import { useSelector, useDispatch } from "react-redux";
import { signOut } from "../store/userSlice";
import MuiAppBar from "@mui/material/AppBar";
const PopupPaper = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(2),
  minWidth: 300,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));
const AppHeader = ({ openMenu, handleDrawerOpen, drawerWidth }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [newAvatar, setNewAvatar] = useState(null);
  const [isHomePage, setIsHomePage] = useState(null);
  const userData = useSelector((state) => state.user);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const isUserSignin = userData.isAuthenticated;

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setNewAvatar(reader.result);
      };
      reader.readAsDataURL(file);
    }
    handlePopoverClose();
  };
  const button = (text, id) => {
    return (
      <Button
        onClick={() => {
          if (id === "home" || id === "dashboard") {
            setIsHomePage(!isHomePage);
          } else if (id.toLowerCase() === "signin") {
            navigate("/user/student/signin");
          } else {
            dispatch(signOut());
          }
        }}
        sx={{
          backgroundColor: "#2196F3",
          color: "#FFFFFF",
          padding: "10px 20px",
          borderRadius: "8px",
          cursor: "pointer",
          fontSize: "16px",
          fontWeight: "bold",
          transition: "background-color 0.3s ease",
          "&:hover": {
            backgroundColor: "#1565C0",
          },
        }}
      >
        {text}
      </Button>
    );
  };
  const getHeaderButtons = () => {
    if (!isUserSignin) {
      return button("Signin", "Signin");
    } else {
      if (isHomePage === null || isHomePage === false) {
        return (
          <>
            {button("Home", "home")}
            <Avatar
              alt="User Avatar"
              src={newAvatar || "/path/to/defaultAvatar.jpg"}
              onClick={handlePopoverOpen}
              sx={{ cursor: "pointer" }}
            />
          </>
        );
      } else {
        return (
          <>
            {button("Dashboard", "dashboard")}
            {button("Signout", "Signout")}
          </>
        );
      }
    }
  };

  const open = Boolean(anchorEl);

  return (
    <AppBar
      position="fixed"
      sx={{
        backgroundColor: "#607D8B",
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
      }}
      open={openMenu}
    >
      <Toolbar sx={{ justifyContent: "space-between" }}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          edge="start"
          sx={{ mr: 2, ...(openMenu && { display: "none" }) }}
        >
          <MenuIcon />
        </IconButton>

        <Typography variant="h6" noWrap component="div">
          {translations.headerTitle}
        </Typography>

        <div
          style={{ position: "relative", display: "flex", columnGap: "1rem" }}
        >
          {getHeaderButtons()}
        </div>

        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handlePopoverClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          sx={{
            borderRadius: "100px",
          }}
        >
          <PopupPaper>
            <Box display="flex" flexDirection="column" alignItems="center">
              <Input
                type="file"
                accept="image/*"
                onChange={handleFileChange}
                sx={{ display: "none" }}
                id="avatar-upload-input"
              />
              <label htmlFor="avatar-upload-input">
                <Avatar
                  alt="User Avatar"
                  src={newAvatar || "/path/to/defaultAvatar.jpg"}
                  sx={{ width: 100, height: 100, cursor: "pointer" }}
                />
                <EditIcon />
              </label>
              <Typography variant="h6" sx={{ marginTop: 2 }}>
                {userData.userName}
              </Typography>
              <Typography variant="body1">Email:{userData.email}</Typography>
              <Typography variant="body1">
                Phone Number:{userData.phoneNumber}
              </Typography>
              <Button variant="contained" color="primary" sx={{ marginTop: 2 }}>
                Edit
              </Button>
            </Box>
          </PopupPaper>
        </Popover>
      </Toolbar>
    </AppBar>
  );
};

export default AppHeader;
