import React, { useEffect, useState } from "react";
import Table from "../../dynamicTable/Table";
import { allColumns, allOptions } from "./usersTableData";
import { GET_USERS_BY_ROLE } from "../../graphql/user";
import { useQuery } from "@apollo/client";
import { prepareGetUsersByRoleVariables } from "../../graphql/variables";
import { useParams } from "react-router-dom";
import { prepareMasterTableData } from "../../utilities/tableData";

export default function UserManagement() {
  const { role } = useParams();

  const { loading, error, data, refetch } = useQuery(
    GET_USERS_BY_ROLE,
    prepareGetUsersByRoleVariables(role)
  );
  const [usersData, setUsersData] = useState([]);

  useEffect(() => {
    if (!loading) {
      // GET_USERS_BY_ROLE
      const { getUsersByRole: getResult } = data;
      if (getResult.success) {
        setUsersData(prepareMasterTableData(getResult.users));
      }
    }
  }, [loading, role]);
  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Table
        columns={allColumns}
        data={usersData}
        header={
          {
            // title,
            // description: "Each set must have 25 questions",
            // style: {
            //   title: {},
            //   description: {},
            //   container: {},
            // },
          }
        }
        actions={null}
        options={allOptions}
        customActions={null}
        detailPanel={null}
      />
    </div>
  );
}
