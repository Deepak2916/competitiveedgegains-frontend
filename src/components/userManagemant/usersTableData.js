export const allColumns = [
  { title: "Name", field: "username" },
  { title: "Email", field: "email" },
  { title: "Phone Number", field: "phoneNumber" },
  { title: "Role", field: "role" },
  { title: "Approved", field: "isApprovedByAdmin" },
];

export const allOptions = {
  paging: false,
  actionsColumnIndex: -1,
};
