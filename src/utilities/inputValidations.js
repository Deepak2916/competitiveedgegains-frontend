import translations from "../en.json";
// Password Validation
function validatePassword(password) {
  const hasCapitalLetter = /[A-Z]/.test(password);

  const hasSpecialCharacter = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]/.test(password);

  const hasMinimumLength = password.length >= 6;

  if (hasCapitalLetter && hasSpecialCharacter && hasMinimumLength) {
    return "";
  } else {
    let errorMessage = translations.passwordRequirements.errorMessage;

    if (!hasCapitalLetter) {
      errorMessage += translations.passwordRequirements.capitalLetter;
    }

    if (!hasSpecialCharacter) {
      errorMessage += translations.passwordRequirements.specialCharacter;
    }

    if (!hasMinimumLength) {
      errorMessage += translations.passwordRequirements.minimumLength;
    }

    return errorMessage;
  }
}

// Phone Number Validation
function validatePhoneNumber(phoneNumber) {
  const phoneRegex = /^(\+91-|\+91|0)?[6-9]\d{9}$/;

  if (phoneRegex.test(phoneNumber)) {
    return "";
  } else {
    return translations.invalidPhoneNumber;
  }
}

function validateConfirmPassword(password, confirmPassword) {
  if (password === confirmPassword) {
    return "";
  } else {
    return translations.passwordNotMatch;
  }
}

// Name Validation
function validateName(name) {
  if (name.length >= 2) {
    return "";
  } else {
    return translations.nameRequirements.minimumLength;
  }
}

// Email Validation
function validateEmail(email) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  if (emailRegex.test(email)) {
    return "";
  } else {
    return translations.invalidMail;
  }
}

function validateCourse(value) {
  if (Array.isArray(value) && value.length) {
    return "";
  } else {
    return translations.selectCourse;
  }
}
export const validateForm = (form) => {
  const errors = {
    userName: validateName(form.userName),
    email: validateEmail(form.email),
    password: validatePassword(form.password),
    phoneNumber: validatePhoneNumber(form.phoneNumber),
    confirmPassword: validateConfirmPassword(
      form.password,
      form.confirmPassword
    ),
  };

  const isFormValid =
    Object.values(errors).find((value) => value.length > 0) === undefined;
  return {
    isFormValid,
    errors,
  };
};

export const validateQuestionForm = (form) => {};
