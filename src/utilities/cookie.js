// cookieUtils.js
import Cookies from "js-cookie";

const COOKIE_NAME = "CEG_TOKEN";

export const setTokenCookie = (token, expires = 1) => {
  Cookies.set(COOKIE_NAME, token, { expires });
};

export const getTokenCookie = () => {
  return Cookies.get(COOKIE_NAME);
};

export const removeTokenCookie = () => {
  Cookies.remove(COOKIE_NAME);
};
