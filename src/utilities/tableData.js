export const prepareMasterTableData = (questionSets) => {
  // {
  //   exam: exam,
  //   subject: subject,
  //   Totalquestions: "1",
  //   totalPoints: "5",
  //   easy: "1",
  //   hard: "1",
  //   medium: "1",
  //   action: "Add",
  //   questionsAdded: "3",
  //   status: "In progress",
  // },
  const result = questionSets.map((data) => ({
    ...data,
  }));
  return result;
};
