import { setContext } from "@apollo/client/link/context";
import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { getTokenCookie } from "./cookie";

const authLink = setContext((_, { headers }) => {
  const token = getTokenCookie();

  return {
    headers: {
      ...headers,
      authorization: token ? token : "",
    },
  };
});

const httpLink = createHttpLink({
  //https://ceg-api.onrender.com/my-graphql-endpoint

  uri: "http://localhost:4000/my-graphql-endpoint",
});

export const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});
